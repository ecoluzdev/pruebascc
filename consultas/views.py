# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from zeep import Client
import zeep
from zeep.wsse.username import UsernameToken

# Create your views here.


def inicio(request):
    return render(request, 'inicio.html')


def consultaCC(request):
    client = Client('./Downloads/ReporteServiceCC_Pruebas.wsdl')
    header = zeep.xsd.Element(
        'Security',
        zeep.xsd.ComplexType([
            zeep.xsd.Element(
                'UsernameToken',
                zeep.xsd.ComplexType([
                    zeep.xsd.Element('ClaveOtorgante', zeep.xsd.String()),
                    zeep.xsd.Element('NombreUsuario', zeep.xsd.String()),
                    zeep.xsd.Element('Password', zeep.xsd.String()),
                ])
            ),
        ])
    )

    header_value = header(UsernameToken={'ClaveOtorgante': '0000081008', 'NombreUsuario': 'IHG8728CCO', 'Password': 'pru3ba00'})


    request_data = {
        'Personas': {
                'Persona': {
                    'DetalleConsulta': {
                        'FolioConsultaOtorgante': '453721',
                        'ProductoRequerido': '1',
                        'TipoCuenta': 'F',
                        'ClaveUnidadMonetaria': 'MX',
                        'ImporteContrato': '4500',
                        'NumeroFirma': '0152250000123456',
                    },
                    'Nombre': {
                        'ApellidoPaterno': 'CERVANTES',
                        'ApellidoMaterno': 'LUNA',
                        'ApellidoAdicional': 'MENDEZ',
                        'Nombres': 'ARACELO',
                        'FechaNacimiento': '1975-05-08', # AAAA-MM-DD
                        'RFC': 'CELA7505082I2',
                        'CURP': 'CELA750508MDFRRNA4',
                        'NAcionalidad': 'MX',
                        'Residencia': '1',
                        'EstadoCivil': 'C',
                        'Sexo': 'F',
                        'ClaveElectorIFE': '',
                        'NumeroDependientes': '2',
                    },
                    'Domicilios': {
                        'Domicilio': {
                            'Direccion': 'RIO AYOTLA MZ 46 LT 9',
                            'ColoniaPoblacion': 'EL SALADO',
                            'DelegacionMunicipio': 'LA PAZ',
                            'Ciudad': '',
                            'Estado': 'DF',
                            'CP': '56524',
                            'FechaResidencia': '1999-05-12',
                            'NumeroTelefono': '5526135704',
                            'TipoDomicilio': 'C',
                            'TipoAsentamiento': '7',
                        },
                    },
                    'Empleos': {
                        'Empleo': {
                            'NombreEmpresa': 'TALLER VULCANIZADORA',
                            'Direccion': 'UNIDAD MIGUEL HIDALGO MZ 5 LT 3',
                            'ColoniaPoblacion': 'TEZOZOMOC',
                            'DelegacionMunicipio': 'AZCAPOTZALCO',
                            'Ciudad': '',
                            'Estado': 'DF',
                            'CP': '02450',
                            'NumeroTelefono': '53821420',
                            'Extension': '',
                            'Fax': '',
                            'Puesto': '',
                            'FechaContratacion': '1999-08-30',
                            'ClaveMoneda': 'MX',
                            'SalarioMensual': '4500',
                            'FechaUltimoDiaEmpleo': '',
                        },
                    },
                    'CuentasReferencia': {
                        'NumeroCuenta': '000110250001053856',
                        'NumeroCuenta': '000110250001053857',
                    },
                },
        },
    }
    response = client.service.generaReporte(
        _soapheaders=[header_value],
        **request_data
    )
    # response = client.service.generaReporte(**request_data)
    return render(request, 'consulta.html')

